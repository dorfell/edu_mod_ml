Machine Learning étude
======================================

Le étude des différents modèles de Machine Learning c'est très important pour déterminer le performance des ceux avec l'utilisation des différentes bases de données, paramètres, et architectures.
Dans ce dépôt vous trouveraz différent preuves avec les graphiques des métriques comme l'exactitude et la perte pour evaluer les Tableau.

#### modèles des  Contenus

- [00 Mnist avec Multi-layer Perceptron](#00_mnist_mlp)
- [00 Mnist avec Multi-layer Perceptron et dropout du 40%](#00_mnist_mlp_drop40)
- [00 Mnist avec Multi-layer Perceptron et dropout du 60%](#00_mnist_mlp_drop60)
- [00 Mnist avec Multi-layer Perceptron et dropout du 80%](#00_mnist_mlp_drop80)
- [00 Mnist avec Multi-layer Perceptron et 1 couche](#00_mnist_mlp_lay1)
- [00 Mnist avec Multi-layer Perceptron et 5 couches](#00_mnist_mlp_lay5)
- [00 Mnist avec Multi-layer Perceptron et 64 neurones per couches](#00_mnist_mlp_neu64)
- [00 Mnist avec Multi-layer Perceptron et 128 neurones per couches](#00_mnist_mlp_neu128)
- [00 Mnist avec Multi-layer Perceptron et 256 neurones per couches](#00_mnist_mlp_neu256)
- [00 Mnist avec Multi-layer Perceptron et 1024 neurones per couches](#00_mnist_mlp_neu1024)
- [01 Mnist avec Réseau Neuronal Convolutif](#01_mnist_cnn)
- [01 Mnist avec Réseau Neuronal Convolutif avec 1 couche](#01_mnist_cnn_lay1)
- [01 Mnist avec Réseau Neuronal Convolutif avec 2 couche](#01_mnist_cnn_lay2)
- [01 Mnist avec Réseau Neuronal Convolutif avec le double de neurones](#01_mnist_cnn_neux2)
- [02 CIFAR10 avec Réseau Neuronal Convolutif](#02_cifar10_cnn)
- [03 FER2013 avec Réseau Neuronal Convolutif](#03_fer2013_cnn)
- [04 Pré-traîtement des bases de données_avec_dlib](#04_pretraitement_dlib)
- [05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 96x96 avec l'optimisateur Adam](#05_jaffe_gray_cnn_m1_96x96)
- [05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128 et l'optimisateur Adam](#05_jaffe_gray_cnn_m1_128x128_Adam)
- [05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur Adam](#05_jaffe_gray_cnn_m1_128x128_aug_Adam)
- [05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128 et l'optimisateur SGD](#05_jaffe_gray_cnn_m1_128x128_SGD)
- [05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur SGD](#05_jaffe_gray_cnn_m1_128x128_aug_SGD)
- [05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur Adam](#05_jaffe_gray_cnn_m2_128x128_aug_Adam)
- [05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128,  l'optimisateur Adam et 100 époques](#05_jaffe_gray_cnn_m2_128x128_aug_Adam_100_epoques)
- [05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur SGD](#05_jaffe_gray_cnn_m2_128x128_aug_SGD)
- [05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation_des_données et l'optimisateur SGD sans le paràmetre steps_per_epoch](#05_jaffe_gray_cnn_m2_128x128_aug_SGD_sans_step_epoch)
- [05 JAFFE avec le modèle M3 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur Adam et 100 époques](#05_jaffe_gray_cnn_m2_128x128_aug_Adam_100_epoques)




# 00 Mnist avec Multi-layer Perceptron (00_mnist_mlp)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec dropout de 40% (00_mnist_mlp_drop40)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec un "dropout" de 40% est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec dropout de 60% (00_mnist_mlp_drop60)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec un "dropout" de 60% est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec dropout de 80% (00_mnist_mlp_drop80)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec un "dropout" de 80% est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec 1 couche (00_mnist_mlp_lay1)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec un couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec 5 couche (00_mnist_mlp_lay5)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec 5 couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec 64 neurones (00_mnist_mlp_neu64)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec 64 neurones per chaque couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec 128 neurones (00_mnist_mlp_neu128)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec 128 neurones per chaque couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec 256 neurones (00_mnist_mlp_neu256)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec 256 neurones per chaque couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 00 Mnist avec Multi-layer Perceptron avec 1024 neurones (00_mnist_mlp_neu1024)
-------------------------------
Dans cette preuve un modèle Multi-layer Perceptron (MLP) avec 1024 neurones per chaque couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 01 Mnist avec Réseau Neuronal Convolutif (CNN) (01_mnist_cnn)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN) est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 01 Mnist avec Réseau Neuronal Convolutif (CNN) avec 1 couche (01_mnist_cnn_lay1)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN) avec 1 couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 01 Mnist avec Réseau Neuronal Convolutif (CNN) avec 2 couche (01_mnist_cnn_lay2)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN) avec 2 couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 01 Mnist avec Réseau Neuronal Convolutif (CNN) avec le double des neurones (01_mnist_cnn_neux2)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN) avec le double nombre de neurones pour chaque couche est évalué avec les métriques du exactitude et perte vs epoques. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 02 CIFAR10 avec Réseau Neuronal Convolutif (CNN)  (02_cifar10_cnn)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN)  est évalué avec les métriques du exactitude et perte vs epoques pour le base de données CIFAR10. Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 03 FER2013 avec Réseau Neuronal Convolutif (CNN)  (03_fer2013_cnn)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN)  est évalué avec les métriques du exactitude et perte vs epoques pour le base de données des expressions facial FER2013 ( Visitez https://www.kaggle.com pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre de epochs (i.e. 5, 10, 15, ..., 30).

# 04 Pretraitement des bases de données avec dlib (04_pretraitement_dlib)
-------------------------------
Ce logiciel permet de faire le pré-traîtment des basses de données (e.g.  Jaffe). Le pré-traîtement est compose des étapes suivantes: Détecter le visage, détecter les yeux avec landmarks (voir dlib bibliothéque), roter le visage, redimensionner et égaliser l'histogramme. La base de données Jaffe peut être téléchagé à l'addresse ci-dessous:  http://www.kasrl.org/jaffe.html

# 05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 96x96 avec l'optimisateur Adam (#05_jaffe_gray_cnn_m1_96x96)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30). Les images sont en échalle des gris de taille 96x96 et l'optimisateur Adam est utilisé.

#05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128 et l'optimisateur Adam (#05_jaffe_gray_cnn_m1_128x128_Adam)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30). Les images sont en échalle des gris de taille 128x128 et l'optimisateur Adam est utilisé.

# 05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur Adam (#05_jaffe_gray_cnn_m1_128x128_aug_Adam)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30). Les images sont en échalle des gris de taille 128x128, il y a augmentation des données (i.e. rotations et flip horizontale) et l'optimisateur Adam est utilisé.

# 05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128 avec l'optimisateur SGD (#05_jaffe_gray_cnn_m1_128x128_SGD)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30) et les images sont en échalle des gris de taille 128x128 et et l'optimisateur SGD est utilisé.

# 05 JAFFE avec le modèle M1 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur SGD (#05_jaffe_gray_cnn_m1_128x128_aug_SGD)
-------------------------------
Dans cette preuve un modèle de Résau Neuronal Convolutif (CNN)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30). Les images sont en échalle des gris de taille 128x128, il y a augmentation des données (i.e. rotations et flip horizontale) et l'optimisateur SGD est utilisé.

#05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur Adam (#05_jaffe_gray_cnn_m2_128x128_aug_Adam)
-------------------------------
Dans cette preuve un modèle M2 (pris de Efficient Facial Expression Recognition Algorithm Based on hierarchical Deep Neural Network Structure, Kim et al., IEEE Access 2019.)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30). Les images sont en échalle des gris de taille 128x128, il y a augmentation des données (i.e. rotations et flip horizontale) et l'optimisateur Adam est utilisé.

# 05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128,  l'optimisateur Adam et 100 époques (#05_jaffe_gray_cnn_m2_128x128_aug_Adam_100_epoques)
-------------------------------
Dans cette preuve un modèle M2 (pris de Efficient Facial Expression Recognition Algorithm Based on hierarchical Deep Neural Network Structure, Kim et al., IEEE Access 2019.)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30, 50, 75, 100). Les images sont en échalle des gris de taille 128x128, il y a augmentation des données (i.e. rotations et flip horizontale) et l'optimisateur Adam est utilisé.

# 05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur SGD (#05_jaffe_gray_cnn_m2_128x128_aug_SGD)
-------------------------------
Dans cette preuve un modèle M2 (pris de Efficient Facial Expression Recognition Algorithm Based on hierarchical Deep Neural Network Structure, Kim et al., IEEE Access 2019.)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30). Les images sont en échalle des gris de taille 128x128, il y a augmentation des données (i.e. rotations et flip horizontale) et l'optimisateur SGD est utilisé.

# 05 JAFFE avec le modèle M2 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation_des_données et l'optimisateur SGD sans le paràmetre steps_per_epoch (#05_jaffe_gray_cnn_m2_128x128_aug_SGD_sans_step_epoch)
-------------------------------
Dans cette preuve un modèle M2 (pris de Efficient Facial Expression Recognition Algorithm Based on hierarchical Deep Neural Network Structure, Kim et al., IEEE Access 2019.)  est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30). Les images sont en échalle des gris de taille 128x128, il y a augmentation des données (i.e. rotations et flip horizontale) et l'optimisateur SGD est utilisé mais sans le passer le paramètre step_per_epoch.


# 05 JAFFE avec le modèle M3 du CNN. images pré-traîtées en échelle des gris du taille 128x128, augmentation des données et l'optimisateur Adam et 100 époques (#05_jaffe_gray_cnn_m2_128x128_aug_Adam_100_epoques)
-------------------------------
Dans cette preuve un modèle M3 (VGG16) (pris de  Very Deep Convolutional Networks for Large-Scale Image Recognition, Simonyan K. et Zisserman A., ICLR 2015) est évalué avec les métriques du exactitude et perte vs époques pour le base de données des expressions facial JAFFE ( Visitez http://www.kasrl.org/jaffe.html pour le télécharger). Les paramètres qui changent sont le batch_size (i.e. 32, 64, 128, 256, 512, 1024) et les nombre des époques (i.e. 5, 10, 15, ..., 30, 50, 75, 100). Les images sont en échalle des gris de taille 128x128, il y a augmentation des données (i.e. rotations et flip horizontale) et l'optimisateur Adam est utilisé.


