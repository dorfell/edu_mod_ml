#!/bin/bash

echo "--------------------------------"
echo "  Changement des epochs ...     "
echo "--------------------------------"

echo "--> Preuve avec  5  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/100/, 5); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep5.pdf

echo "--> Preuve avec  10  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/5/, 10); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep10.pdf

echo "--> Preuve avec  15  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/10/, 15); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep15.pdf

echo "--> Preuve avec  20  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/15/, 20); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep20.pdf

echo "--> Preuve avec  25  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/20/, 25); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep25.pdf

echo "--> Preuve avec  30  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/25/, 30); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep30.pdf

echo "--> Preuve avec  50  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/30/, 50); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep50.pdf

echo "--> Preuve avec  75  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/50/, 75); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep75.pdf

echo "--> Preuve avec  100  epochs ..."
echo "--------------------------------"
gawk -i inplace '/epochs/{ gsub(/75/, 100); }1' 03_jaffe_lbp_cnn.py 
echo "--> Exécution du preuve ..."
sh 02_report.sh  
mv reportage_entrainement.pdf reportage_entrainement_ep100.pdf
