#!/bin/bash

echo "========================="
echo "Étuder le CNN avec FER2013 "
echo "========================="

echo "--------------------------------"
echo "  Changement du batch size ...  "
echo "--------------------------------"
echo "--> Effacer les données anciennes ..."
rm  -r batch_size_* 

echo "--> Preuve avec  batch size: 32  ..."
gawk -i inplace '/batch_size/{ gsub(/1024/, 32); }1' 03_fer2013_cnn.py 
mkdir batch_size_32 &&  cp 01_epochs.sh 02_report.sh 03_fer2013_cnn.py batch_size_32/
cd batch_size_32 && sh 01_epochs.sh && cd ..

echo "--> Preuve avec  batch size: 64  ..."
gawk -i inplace '/batch_size/{ gsub(/32/, 64); }1' 03_fer2013_cnn.py 
mkdir batch_size_64 &&  cp 01_epochs.sh 02_report.sh 03_fer2013_cnn.py batch_size_64/
cd batch_size_64 && sh 01_epochs.sh && cd ..

echo "--> Preuve avec  batch size: 128  ..."
gawk -i inplace '/batch_size/{ gsub(/64/, 128); }1' 03_fer2013_cnn.py 
mkdir batch_size_128 &&  cp 01_epochs.sh 02_report.sh 03_fer2013_cnn.py batch_size_128/
cd batch_size_128 && sh 01_epochs.sh && cd ..

echo "--> Preuve avec  batch size: 256  ..."
gawk -i inplace '/batch_size/{ gsub(/128/, 256); }1' 03_fer2013_cnn.py 
mkdir batch_size_256 &&  cp 01_epochs.sh 02_report.sh 03_fer2013_cnn.py batch_size_256/
cd batch_size_256 && sh 01_epochs.sh && cd ..

echo "--> Preuve avec  batch size: 512  ..."
gawk -i inplace '/batch_size/{ gsub(/256/, 512); }1' 03_fer2013_cnn.py 
mkdir batch_size_512 &&  cp 01_epochs.sh 02_report.sh 03_fer2013_cnn.py batch_size_512/
cd batch_size_512 && sh 01_epochs.sh && cd ..

echo "--> Preuve avec  batch size: 1024  ..."
gawk -i inplace '/batch_size/{ gsub(/512/, 1024); }1' 03_fer2013_cnn.py 
mkdir batch_size_1024 &&  cp 01_epochs.sh 02_report.sh 03_fer2013_cnn.py batch_size_1024/
cd batch_size_1024 && sh 01_epochs.sh && cd ..
