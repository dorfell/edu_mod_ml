
from __future__ import print_function

import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D
from keras.layers import Dense, Activation, Dropout, Flatten

from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

from keras import backend as K
from keras.utils import plot_model

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np


# Variables
#------------------------------
num_classes = 7 #angry, disgust, fear, happy, sad, surprise, neutral
batch_size = 1024
epochs = 30
#epochs = 2

print(batch_size,  'batch_size');
print(epochs,      'epochs');
print(num_classes, 'num_classes');


# Read kaggle facial expression recognition challenge dataset (fer2013.csv)
# https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge
#------------------------------
with open("../../db/fer2013/fer2013.csv") as f:
    content = f.readlines()

lines = np.array(content)

num_of_instances = lines.size
print("number of instances: ",num_of_instances)
print("instance length: ",len(lines[1].split(",")[1].split(" ")))


# Initialize trainset and test set
#------------------------------
x_train, y_train, x_test, y_test = [], [], [], []


# Transfer train and test set data
#------------------------------
for i in range(1,num_of_instances):
    try:
        emotion, img, usage = lines[i].split(",")

        val = img.split(" ")

        pixels = np.array(val, 'float32')

        emotion = keras.utils.to_categorical(emotion, num_classes)

        if 'Training' in usage:
            y_train.append(emotion)
            x_train.append(pixels)
        elif 'PublicTest' in usage:
            y_test.append(emotion)
            x_test.append(pixels)
    except:
        print("",end="")

x_test_orig = x_test; y_test_orig = y_test;


# Data transformation for train and test sets
#------------------------------
x_train = np.array(x_train, 'float32')
y_train = np.array(y_train, 'float32')
x_test = np.array(x_test, 'float32')
y_test = np.array(y_test, 'float32')

x_train /= 255 # normalize inputs between [0, 1]
x_test /= 255

x_train = x_train.reshape(x_train.shape[0], 48, 48, 1)
x_train = x_train.astype('float32')
x_test = x_test.reshape(x_test.shape[0], 48, 48, 1)
x_test = x_test.astype('float32')

print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')


# Construct CNN structure
#------------------------------
model = Sequential()

# 1st convolution layer
model.add(Conv2D(64, (5, 5), activation='relu', input_shape=(48,48,1)))
model.add(MaxPooling2D(pool_size=(5,5), strides=(2, 2)))

# 2nd convolution layer
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(AveragePooling2D(pool_size=(3,3), strides=(2, 2)))

# 3rd convolution layer
model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(AveragePooling2D(pool_size=(3,3), strides=(2, 2)))
model.add(Flatten())

# fully connected neural networks
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(num_classes, activation='softmax'))

model.summary();
plot_model(model, to_file="model.png", show_shapes="True");


# Batch process
#------------------------------
datagen = ImageDataGenerator()
#train_generator = datagen.flow(x_train, y_train, batch_size=batch_size)


# Training
#------------------------------
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adam(),
              metrics=['accuracy'])


fit = True
if fit == True:
        # history = model.fit_generator(x_train, y_train,                                                                    epochs=epochs,                                                                       verbose=2) # train for all trainset
        history = model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size), epochs=epochs, verbose=2, validation_data=(x_test, y_test), workers=4); # train for randomly selected one
else:
        model.load_weights('data/facial_expression_model_weights.h5') # load weights


# Evaluating
#------------------------------
"""
# overall evaluation
score = model.evaluate(x_test, y_test)

"""
#------------------------------
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

predictions = model.predict(x_test);
predicted_label = np.argmax(predictions[0]);

#print(history.history);
print(history.history.keys());



# Function for drawing bar chart for emotion preditions
"""def emotion_analysis(emotions):
#    objects = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
#    y_pos = np.arange(len(objects))
    plt.bar(y_pos, emotions, align='center', alpha=0.5)
    plt.title('emotion')
    plt.xticks(y_pos, objects)
    plt.ylabel('percentage'); plt.xlabel('Emotion');
    export_pdf.savefig(); plt.close();
"""

with PdfPages("entrainement_graphique.pdf")  as export_pdf:
    
    # summarize history for accuracy
    plt.plot(history.history['accuracy']);
    plt.plot(history.history['val_accuracy']);
    plt.title('model accuracy');
    plt.ylabel('accuracy'); plt.xlabel('epoch');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();
    
    # summarize history for loss
    plt.plot(history.history['loss']);
    plt.plot(history.history['val_loss']);
    plt.title('model loss');
    plt.ylabel('loss'); plt.xlabel('epoch');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();

    # Make prediction for custom image out of test set
    img = image.load_img("../../db/dataset/jackman.png", color_mode="grayscale", target_size=(48, 48))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis = 0)
    x /= 255;
    x_orig = x;

    # Exemple
    x = np.array(x, 'float32')
    x = x.reshape([48, 48]);
    #plt.gray()
    plt.imshow(x,  cmap=plt.cm.binary);
    plt.colorbar(); plt.grid(False);
    plt.title('Donnée exemple');
    plt.ylabel(''); plt.xlabel("expresion du exemple");
    export_pdf.savefig(); plt.close();

    # Predictions
    custom = model.predict(x_orig)
    objects = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
    y_pos = np.arange(len(objects))
    plt.bar(y_pos, custom[0], align='center', alpha=0.5)
    plt.title('Predictions')
    plt.xticks(y_pos, objects)
    plt.ylabel('percentage'); plt.xlabel('Emotion');
    export_pdf.savefig(); plt.close();


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
                                                



