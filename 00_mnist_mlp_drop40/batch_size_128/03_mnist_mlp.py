'''Trains a simple deep NN on the MNIST dataset.

Gets to 98.40% test accuracy after 30 epochs
(there is *a lot* of margin for parameter tuning).
2 seconds per epoch on a K520 GPU.
'''
from __future__ import print_function

import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop

from keras.utils import plot_model

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np


batch_size = 128;
epochs = 30;
num_classes = 10;

print(batch_size,   'batch_size');
print(epochs,      'epochs');
print(num_classes, 'num_classes');

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data();
x_test_orig = x_test; y_test_orig = y_test;

x_train = x_train.reshape(60000, 784);
x_test = x_test.reshape(10000, 784);
x_train = x_train.astype('float32');
x_test = x_test.astype('float32');
x_train /= 255;
x_test /= 255;
print(x_train.shape[0], 'train samples');
print(x_test.shape[0], 'test samples');

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes);
y_test = keras.utils.to_categorical(y_test, num_classes);

model = Sequential();
model.add(Dense(512, activation='relu', input_shape=(784,)));
model.add(Dropout(0.4));
model.add(Dense(512, activation='relu'));
model.add(Dropout(0.4));
model.add(Dense(num_classes, activation='softmax'));

model.summary();

plot_model(model, to_file="model.png", show_shapes="True");

model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(),
              metrics=['accuracy']);

history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=2,
                    validation_data=(x_test, y_test));


score = model.evaluate(x_test, y_test, verbose=0);
print('Test loss:', score[0]);
print('Test accuracy:', score[1]);

predictions = model.predict(x_test);
predicted_label = np.argmax(predictions[0]);

#print(history.history);
print(history.history.keys());

with PdfPages("entrainement_graphique.pdf")  as export_pdf:
    
    # summarize history for accuracy
    plt.plot(history.history['accuracy']);
    plt.plot(history.history['val_accuracy']);
    plt.title('model accuracy');
    plt.ylabel('accuracy'); plt.xlabel('epoch');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();
    
    # summarize history for loss
    plt.plot(history.history['loss']);
    plt.plot(history.history['val_loss']);
    plt.title('model loss');
    plt.ylabel('loss'); plt.xlabel('epoch');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();

    # Exemple
    plt.imshow(x_test_orig[0],  cmap=plt.cm.binary);
    plt.colorbar(); plt.grid(False);
    plt.title('Donnée exemple');
    plt.ylabel(''); plt.xlabel(y_test_orig[0]);
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();

    # Predictions
    plt.bar(range(10), predictions[0], color="#777777");
    plt.title('Predicitions');
    #plt.ylabel(''); plt.xlabel("");
    plt.xticks(range(10)); plt.yticks([]);
    export_pdf.savefig(); plt.close();
    
print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
