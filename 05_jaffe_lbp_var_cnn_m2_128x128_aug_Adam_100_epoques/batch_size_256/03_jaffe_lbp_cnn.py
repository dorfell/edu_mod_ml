# -*- coding: utf-8 -*-
## 
# @file    
# @brief   FER avec CNNs: JAFFE_LBP_CNN 
# @details Ce module permettre l'entraînament d'un CNN avec
#          le base de données JAFFE et les images prè-traîtées
#          et transformées avec le motif binaire local LBP méthode: var. 
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/03/03
# @version 0.1
"""@package docstring
"""
################################
## 03_jaffe_lbp_cnn
## by Dorfell Parra
## 2020/03/03
#################################

import keras
import numpy as np

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D
from keras.layers import Dense, Activation, Dropout, Flatten

from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

from keras import backend as K
from keras.utils import plot_model

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


# Variables
#------------------------------
# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];
num_classes = 7 #angry, disgust, fear, happy, sad, surprise, neutral
batch_size = 256
epochs = 100

print(batch_size,  'batch_size');
print(epochs,      'epochs');
print(num_classes, 'num_classes');

# Charger données dès fichiers binaires *.npy
img_dat = np.load("../../db/don_traite_jaffe_dlib_128x128_lbp_var.npy"); 
img_lab = np.load("../../db/lab_traite_jaffe_dlib_128x128_lbp_var.npy"); 

#print("Img dat: ", img_dat, img_dat.shape, type(img_dat), img_lab);
print("Img dat: ", img_dat.shape, type(img_dat));
print("Img lab: ", img_lab.shape, type(img_lab));

# Dessiner quelques données
#fig=plt.figure(figsize=(16, 150));
#cols = 2;  rows = 2;
#for i in range(1, cols*rows + 1):
#    img = np.squeeze(img_dat[i]);
#    ax  = fig.add_subplot(rows, cols, i);
#    lab = fer_eti[img_lab[i]];
#    ax.set_xlabel(lab);
#    plt.imshow(img, cmap = 'gray');
#plt.subplots_adjust(top=0.9)
#plt.show();


# Data transformation for train and test sets
#------------------------------
# 10-fold cross validation
# The entire dataset is divided into 10 sets, 9 for training, 1 for testing
x_train = np.array(img_dat[0:190],   "float32");
y_train = np.array(img_lab[0:190],   "float32"); 
y_train = keras.utils.to_categorical(y_train, num_classes); # One-hot encoded

x_test  = np.array(img_dat[190:214], "float32");
y_test  = np.array(img_lab[190:214], "float32"); 
y_test  = keras.utils.to_categorical(y_test, num_classes); # One-hot encoded

x_train /= 255; # normalize inputs between [0, 1]
x_test  /= 255; # normalize inputs between [0, 1]

print(x_train.shape[0], 'train samples');
print(x_test.shape[0],  'test samples' );


# Construct CNN structure
#------------------------------
model = Sequential();

# 1st convolution layer
model.add(Conv2D(64, (5, 5), padding="same", activation="relu", input_shape=(128,128,1)));
model.add(MaxPooling2D(pool_size=(2,2), strides=(2, 2)));

# 2nd convolution layer
model.add(Conv2D(128, (5, 5), padding="same", activation="relu"));
model.add(AveragePooling2D(pool_size=(2,2), strides=(2, 2)));

# 3rd convolution layer
model.add(Conv2D(256, (5, 5), padding="same", activation="relu"));
model.add(AveragePooling2D(pool_size=(2,2), strides=(2, 2)));
model.add(Flatten());

# fully connected neural networks
model.add(Dense(1024, activation='relu'));
model.add(Dropout(0.2));
model.add(Dense(512, activation='relu'));
model.add(Dropout(0.2));
model.add(Dense(num_classes, activation='softmax'))

model.summary();
plot_model(model, to_file="model.png", show_shapes="True");


# Batch process, augmentation de données
#------------------------------
#datagen = ImageDataGenerator(); # Sans augmentation
datagen = ImageDataGenerator(
          rotation_range=5,
          #width_shift_range=0.2,
          #height_shift_range=0.2,
          #zoom_range=0.2,
          horizontal_flip=True);


# Training
#------------------------------
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adam(), metrics=['accuracy']);


fit = True;
if fit == True:
        # history = model.fit_generator(x_train, y_train, epochs=epochs, verbose=2) # train for all trainset
        history = model.fit_generator(
                  datagen.flow(x_train, y_train, batch_size=batch_size),
                  steps_per_epoch = np.ceil(len(x_train) / batch_size), 
                  epochs=epochs, verbose=2, 
                  validation_data=(x_test, y_test), workers=4); # train for randomly selected one
else:
        model.load_weights('data/facial_expression_model_weights.h5') # load weights


# Evaluating
#------------------------------
"""
# overall evaluation
score = model.evaluate(x_test, y_test)

"""
#------------------------------
score = model.evaluate(x_test, y_test, verbose=0);
print('Test loss:', score[0]);
print('Test accuracy:', 100*score[1]);

predictions = model.predict(x_test);
predicted_label = np.argmax(predictions[0]);

#print(history.history);
print(history.history.keys());


# Fonction qui sert à mesurer le performance du modèle 
with PdfPages("entrainement_graphique.pdf")  as export_pdf:
    
    # summarize history for accuracy
    plt.plot(history.history['accuracy']);
    plt.plot(history.history['val_accuracy']);
    plt.title('model accuracy lbp var');
    plt.ylabel('accuracy'); plt.xlabel('epoch');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();
    
    # summarize history for loss
    plt.plot(history.history['loss']);
    plt.plot(history.history['val_loss']);
    plt.title('model loss lbp var');
    plt.ylabel('loss'); plt.xlabel('epoch');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();




print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
