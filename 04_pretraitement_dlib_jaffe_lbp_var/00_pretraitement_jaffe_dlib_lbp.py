# -*- coding: utf-8 -*-
## 
# @file    
# @brief   Pretraîtment des images + LBP
# @details Ce module fait les pretraîtement des images des base de données
#          pour amèliorer l'estimation des expressions. En plus, il fait 
#          le motif binaire local LBP aux images.	
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/02/16
# @version 0.1
"""@package docstring
"""
################################
## 00_Pretraîtement_lbp
## by Dorfell Parra
## 2020/02/16
#################################

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages

# Localisation de base de données (db)
loc_db = "../db/jaffedbase/"; 


# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];


# Lire la base de données
img_dat, img_lab = read_data(loc_db);
#print(img_dat, img_dat.shape,  img_lab);


# Pre-traitement des données
img_dat_resized = preprocessing(img_dat);
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);


# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;             # Rayon de cercle
n_points = 8 * radius;  # nombre de points voisins circulairement symétriques.
method= "var";          # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.


img_res_lbp = [];
for img in img_dat_resized:
    img_res_lbp.append(lbp_fun(img, n_points, radius, method));
#print("img_res_lbp:", img_res_lbp, len(img_res_lbp) );


#  Creér un tensor 4D
dat_4D = [];

for idx, ele in enumerate(img_res_lbp):
    #print("print  ele: ", type(ele),  ele.shape);
    tmp_4D  = np.expand_dims(ele, axis=2); # En ajoutant 1 dimension per chaque element.
    #print("img_tmp: ", img_tmp, type(img_tmp), img_tmp.shape);
    dat_4D.append( tmp_4D);

dat_4D = np.array(dat_4D);
#print("dat_4D: ", dat_4D, type(dat_4D), dat_4D.shape);


# Dessiner quelques données
#fig=plt.figure(figsize=(16, 150));
#cols = 2;  rows = 2;
#for i in range(1, cols*rows + 1):
#    img = np.squeeze(dat_4D[i-1]);
#    ax  = fig.add_subplot(rows, cols, i);
#    lab = fer_eti[img_lab[i-1]];
#    ax.set_xlabel(lab);
#    plt.imshow(img, cmap = 'gray');
#plt.subplots_adjust(top=0.9);
#plt.show();


# Fonction qui sert à mesurer le performance du modèle 
with PdfPages("exemple_lbp_var.pdf")  as export_pdf:
 
    #fig=plt.figure(figsize=(16, 150));
    fig=plt.figure();
    cols = 2;  rows = 2;
    for i in range(1, cols*rows + 1):
        img = np.squeeze(dat_4D[i-1]);
        ax  = fig.add_subplot(rows, cols, i);
        lab = fer_eti[img_lab[i-1]];
        ax.set_xlabel(lab);
        plt.imshow(img, cmap = 'gray');
    plt.subplots_adjust(top=0.9);
    export_pdf.savefig(); plt.close();




# Sauver à fichier binaire *.npy
# Changer aussi dans mes_fonctions.py 
np.save("../db/don_traite_jaffe_dlib_128x128_lbp_var.npy", dat_4D );
np.save("../db/lab_traite_jaffe_dlib_128x128_lbp_var.npy", img_lab);


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
